package internal

import (
	"bytes"
	"fmt"
	"html/template"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/mailgun/mailgun-go"
	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/html"
)

const MAILGUN_DOMAIN string = "MAILGUN_DOMAIN"
const DEFAULT_DOMAIN string = "localhost"
const MAILGUN_API_KEY string = "MAILGUN_API_KEY"
const EMAIL_FROM_ADDR string = "EMAIL_FROM_ADDRESS"
const DEFAULT_FROM_ADDR string = "noreply"
const MAIL_TO string = "EMAIL_TO_ADDRESSES"
const SEND_HTML_EMAIL = "SEND_HTML_EMAIL"

type Mailer interface {
	initMailer(domain string, apiKey string)
	sendMail(formValues map[string][]string)
}

type ConsoleMailer struct {
}

type MailgunMailer struct {
	mailerImpl               mailgun.Mailgun
	fromAddress, toAddresses string
}

func SendEmail(formValues map[string][]string) {
	mailGunApiKey := os.Getenv(MAILGUN_API_KEY)
	mailerDomain := os.Getenv(MAILGUN_DOMAIN)
	if mailerDomain == "" {
		mailerDomain = DEFAULT_DOMAIN
	}

	//Yes, this is a constructor pattern from OOP.  No, I'm not sorry I borrowed it.
	var mailer Mailer
	switch {
	case mailGunApiKey != "":
		mailer = new(MailgunMailer)
		mailer.initMailer(mailerDomain, mailGunApiKey)
	default:
		mailer = new(ConsoleMailer)
		mailer.initMailer(mailerDomain, "")
	}

	mailer.sendMail(formValues)

}

func (m *ConsoleMailer) initMailer(domain string, apiKey string) {

}

func (m *ConsoleMailer) sendMail(formValues map[string][]string) {
	fmt.Println(formatMail(formValues))
}

func (m *MailgunMailer) initMailer(domain string, apiKey string) {
	m.mailerImpl = mailgun.NewMailgun(domain, apiKey)
	m.fromAddress = os.Getenv(EMAIL_FROM_ADDR)
	if m.fromAddress == "" {
		m.fromAddress = DEFAULT_FROM_ADDR + "@" + domain
	}
	m.toAddresses = os.Getenv(MAIL_TO)
}

func (m *MailgunMailer) sendMail(formValues map[string][]string) {
	sendHtmlMailSetting := os.Getenv(SEND_HTML_EMAIL)
	var sendHtmlEmail bool
	var body string
	switch sendHtmlMailSetting {
	case "Yes":
		sendHtmlEmail = true
	case "yes":
		sendHtmlEmail = true
	case "True":
		sendHtmlEmail = true
	case "true":
		sendHtmlEmail = true
	default:
		sendHtmlEmail = false
	}
	template, err := template.ParseFiles("templates/email.tmpl")
	if err != nil {
		panic(err)
	}

	body = formatMail(formValues)

	message := m.mailerImpl.NewMessage(m.fromAddress, "You have received a form", body, m.toAddresses)
	if sendHtmlEmail {
		var buffer bytes.Buffer
		minifier := minify.New()
		minifier.AddFunc("text/html", html.Minify)
		template.Execute(&buffer, gin.H{"formValues": formValues})

		html, err := minifier.String("text/html", buffer.String())
		if err != nil {
			panic(err)
		}
		message.SetHtml(html)
	}
	_, _, err = m.mailerImpl.Send(message)

	if err != nil {
		panic(err)
	}
}

func formatMail(formValues map[string][]string) string {
	var valueString string
	for key, value := range formValues {
		valueString += fmt.Sprintln(key, value)
	}
	body := fmt.Sprintf("Someone has submitted the following values to your form:\n%s", valueString)
	return body
}
