package internal

import (
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/meyskens/go-hcaptcha"
)

const HCAPTCHA_API_KEY string = "HCAPTCHA_API_KEY"
const HCAPTCHA_VALIDATE_IP string = "HCAPTCHA_VALIDATE_IP"

type CaptchaValidator interface {
	//Drop init - Pass in a usable validator (newValidator function)
	Init(apiKey string)
	Validate(captchaResponse string, ipAddress string) bool
}

type HCaptchaValidator struct {
	apiKey        string
	validatorImpl hcaptcha.HCaptcha
}

type AnythingGoesValidator struct {
}

func (v *HCaptchaValidator) Init(apiKey string) {
	v.apiKey = apiKey
	v.validatorImpl = *hcaptcha.New(apiKey)
}

func (v *HCaptchaValidator) Validate(captchaResponse string, ipAddress string) bool {
	response, err := v.validatorImpl.Verify(captchaResponse, ipAddress)
	if err != nil {
		panic(err)
	}
	return response.Success
}

func (v *AnythingGoesValidator) Init(apiKey string) {
}

func (v *AnythingGoesValidator) Validate(captchaResponse string, ipAddress string) bool {
	return true
}

func ValidateCaptcha(context *gin.Context) bool {
	var validator CaptchaValidator
	var captchaResult, ip string
	validateIp, _ := strconv.ParseBool(os.Getenv(HCAPTCHA_VALIDATE_IP))

	if validateIp {
		ip = strings.Split(context.Request.RemoteAddr, ":")[0]
	}

	hCaptchaApiKey := os.Getenv(HCAPTCHA_API_KEY)
	switch {
	case hCaptchaApiKey != "":
		validator = new(HCaptchaValidator)
		validator.Init(hCaptchaApiKey)
		captchaResult = context.Request.Form.Get("h-captcha-response")
	default:
		validator = new(AnythingGoesValidator)
	}

	return validator.Validate(captchaResult, ip)
}
