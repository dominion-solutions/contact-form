package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/dominion-solutions/contact-form/internal"
)

//Go learn about Iota. - Keyword in Go (Really great for Bit Flags)

//Type is implied for Constants. - Numeric are untyped (Typed on necessity)
//Structure feels very Java-y - Don't forget directories are packages.  Don't break stuff up until you have to.
const (
	PORT             = "PORT"
	DEFAULT_PORT     = "3000"
	FORWARD_RESPONSE = "forward-to"
)

func main() {
	// Could use a configuration loader library instead.
	port := os.Getenv(PORT)
	if port == "" {
		port = DEFAULT_PORT
	}

	router := setupRouter()
	initRoutes(router)
	router.Run(":" + port)
}

func setupRouter() *gin.Engine {
	router := gin.Default()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("templates/*.tmpl")
	router.Static("/static/", "./static")
	return router
}

func initRoutes(router *gin.Engine) {
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})
	})

	router.POST("/submit", handleForm)
}

func handleForm(context *gin.Context) {
	context.Request.ParseMultipartForm(context.Request.ContentLength)
	if internal.ValidateCaptcha(context) {
		formValues := context.Request.PostForm
		formValues.Del("g-recaptcha-response")
		formValues.Del("h-captcha-response")
		internal.SendEmail(formValues)
		context.HTML(http.StatusOK, "email.tmpl", gin.H{
			"formValues": context.Request.PostForm,
		})
	}

	forwardTo := context.Request.Form.Get(FORWARD_RESPONSE)
	if forwardTo != "" {
		context.Redirect(http.StatusPermanentRedirect, forwardTo)
	}
	context.JSON(http.StatusBadRequest, gin.H{})
}
