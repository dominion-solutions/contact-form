FROM golang:1.17-alpine

LABEL maintainer="Mark Horninger <mark.horninger@dominion.solutions>"

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main .

EXPOSE 8080

CMD [ ./main ]